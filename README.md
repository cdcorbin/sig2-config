# README #

This repository contains configurations for the SigII experiments, and the agent lists used by the scripts in the [vscripts](https://bitbucket.org/cdcorbin/vscripts "vscripts") repository.

## Instructions for running an experiment ##

Follow the steps below to run an experiment on SigII. If no code or configuration needs to be updated, simply start at Step 3, and follow remaining directions, skipping Step 5.

### Step 1 - Clone repositories ###

Make sure you have cloned a copy of each of the following repositories to your local machine:

+ [vpubsub](https://bitbucket.org/cdcorbin/vpubsub "vpubsub")
+ [vmarket](https://bitbucket.org/cdcorbin/vmarket "vmarket") 
+ [vmodels](https://bitbucket.org/cdcorbin/vmodels "vmodels") 
+ [vtrxhvac](https://bitbucket.org/cdcorbin/vtrxhvac "vtrxhvac") 
+ [vscripts](https://bitbucket.org/cdcorbin/vscripts "vscripts") 
+ this respository

Note: If you download the repository instead of cloning using "git clone [URL]", then you will need to rename the downloaded folders to the original repository name, e.g. "vpubsub".

### Step 2 - Transfer files to SigII VOLTTRON ###

Using your SFTP client, copy the repositories just cloned to the volttron user's home directory. This should be "/home/volttron"

### Step 3 - Login to SigII VOLTTRON ###

SSH into the SIGII VOLTTRON board

`ssh volttron@192.168.134.39`

### Step 4 - Activate VOLTTRON virtual environment ###

`$ . volttron/env/bin/activate`

### Step 5 - Install required packages ###

Navigate to the "vpubsub" repository and install it.

```
$ cd /home/volttron/vpubsub
$ python setup.py install
```

Repeat for the "vmarket", "vmodels" and "vtrxhvac" repositories.

### Step 6 - Create symlink to experiment configuration ###

Navigate to the "sig2-config" directory.

`$ cd /home/volttron/sig2-config`

Create symlink.

`$ ln -sfn config_sig2_fixed_2016-11-10 config`

The folder "config_sig2_fixed_2016-11-10" could be any folder containing a particular configuration for an experiment. I will try to follow the naming convention:

`config_[building]_[experiment type]_[date]`

where date is YYYY-MM-DD.

### Step 7 - Install and run the agents ###

From the "sig2-config" directory, use the following script to install and start the agents

`$ . ../vscripts/make-agent-group.sh ../volttron scheduled_sig2.txt`

See [vscripts](https://bitbucket.org/cdcorbin/vscripts "vscripts") for more info on this script.

### Step 8 - Start control ###

The experiment needs to know when to start controlling the building. From the "sig2-config" directory run the following command to start control:

`$ . ../vscripts/make-agent-group.sh ../volttron start_message.txt`

## Instructions for ending an experiment ##

Follow the steps below to run an experiment on SigII.

### Step 1 - Stop control ###

When you are ready to end the experiment, navigate to the "sig2-config" directory.

`$ cd /home/volttron/sig2-config`

Send the kill message to revert all points and end control of the building. Run the following command to end control.

`$ . ../vscripts/make-agent-group.sh ../volttron kill_message.txt`

### Step 2 - Stop agents ###

From the "sig2-config" directory run the following commands to stop the agents:

```
$ . ../vscripts/stop-agent-group.sh scheduled_sig2.txt
$ . ../vscripts/stop-agent-group.sh start_message.txt
$ . ../vscripts/stop-agent-group.sh kill_message.txt
```